# I1

## Bloc 3 : Piloter l'informatique décisionnel d'un S.I (Big Data & Business Intelligence)

### RGPD

[Cours](cours/rgpd.md)

[Evaluation](evaluation/rgpd_evaluation_protection_DCP_GOURIOU_BRICAUD.pdf)

### Data Managment

[Cours](cours/data_management.md)

[NASA API project](https://gitlab.com/Bob-117/nasa-data-management)


### Data Quality

[Cours](cours/data_quality.md)

[RADAR project](https://gitlab.com/Bob-117/data-quality)

### Data Integration

[Cours](cours/data_integration.md)

[Docker Monitoring](https://gitlab.com/Bob-117/pydockermonitoring)

### Data Science & Machine Learning

[Cours](cours/data_machine_learning.md)

[Gladiator project](https://gitlab.com/Bob-117/Gladiator)

### Data Mining

[Cours](cours/data_mining.md)

### Big Data

[Cours](cours/big_data.md)

### Projets et ateliers

[Olympics](https://gitlab.com/Bob-117/olympics)

[Data visualisation](https://gitlab.com/Bob-117/atl_visu_donnees_epsi_i1)


## Bloc 4 : Concevoir & Développer des solutions applicatives métiers et spécifiques

### Analyse des besoins

[badge control](gitlab.com/mistayan/badge-Control/)

### Architecture applicative

[Cours](cours/architecture_applicative.md)

### Security By Design

[Cours](cours/security_by_design.md)


