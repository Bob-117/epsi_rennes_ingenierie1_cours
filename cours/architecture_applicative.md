# Architecture applicative (Marc Gilbert)

- Présentation
- Métier
- Accès aux données

## Location de licorne

Entreprise de location de licorne, gestion du troupeau, des attributs des licornes (vitesse de vol, robe...), gestion des locations, les clients viennent sur place.

- Gestion des licornes
- Gestion de l'agenda
- Gestion des clients


### Architecture 1-tier 

Unique utilisateur, Gestion dans EXCEL

### Architecture 2-tiers

L'entreprise a du succès, on garde un seul troupeau mais géré par plusieurs agences.

Extraction de la couche accès au données

### Architecture 3-tiers n-tiers

- Composants distincts :
    - Client léger poste utilisateur pour la couche présentation
    - Un ou plusieurs serveurs applicatifs pour la logique métier
    - Un ou plusieurs serveurs de données

- La logique métier et l'accès au données peuvent etre réutilisés pour différentes applications

- Systeme complexes, orienté service
- Necessite d'avantage de ressources (serveurs...)

### Dropbox

![](../asset/licorne.svg)


### Evolution avec le client

- Utilisation en ligne
    - Recherche des licornes disponibles
    - Paiement de la location sur le site

![](../asset/licorne-n-tiers.svg)

![](../asset/schema_archi_appli_n_tiers.png)


### Architecture orientée services

SOA : modele d'interaction entre différents systemes

But : rendre réutilisable des composa,ts via : 
- Format d'échange (historiquement xml)
- Couplage lache via une couche d'interface
- Echange synchrone et asynchrone
- Annuaire des services (UDDI Universal Description, Discovery & Integration)


Entreprise Service Bus

- Médiation
- Transformation, aggrégation
- Gestion des versions de services
- Disponibilité

*eg : Apache ServiceMox, Software AGWeb*


### Contrat de service

- Format d'échange
- Interface (web sevrice, fichiers)
- Sécurité (authentification, identification, droits...)
- Performances (volumétrie, temps de réponse, limitation quantité de données ou nb appels)
- Garanties (disponibilité, support)


### SOAP

- Simple Object Access Protocol
- Basé sur XMl
- Définition des échanges dans un WSDL (Web Services Description Language) :
    - Port (url)
    - Binding (protocol de transport)
    - PortType (liste des operations disponibles)
    - Operations (fonctionnalité, params requis, format de réponse, format erreur)
    - Message
    - Type
    - Service (ensemble des ports)

### REST (Representational State Transfer)

- HTTP
- JSON (en général)
- Responsabilité du développeur de faire qqch de propre

Methods : Head, Get, Post, Put, Patch (peu utilisé, souvent bloqué par les proxy), Delete

### Brokers

- Broker de message 
- Traitement asynchrone
- Files (Queues) ou Topic (pub sub, la consommation d'un msg n'entraine pas sa suppression)
- Fournisseur, consomateur

*eg : MQ, RabbitMQ, Kafka*

### Manipulation

git clone https://gitlab.com/formation-pro1/courses/rent-a-unicorn

Utilisation de SoapUI (postman pour soap)

### Evolution

![](../asset/schema_pme_into_mutlinationale.png)

![](../asset/licorne-3.svg)


### MicroService

Choix possible : c'est l'UI qui aggrège les résultats de plusieurs microservices (/!\ asychrone) vs appeler un microservice parent

### Design pattern : MVC

### Orienté objet