### Intro

- Modele KDD (Knowledge Discovery in Databases)
- Modele SEMMA
- Modele CRISP-DM (CRoss Industry Standard Process for Data Mining)

### Processus
Collecte & Préparation

Quantité & Qualité

Analyse et Exploitation

### Clustering
- K-Means (centres itératifs)
- K-Medoids (centre réel)
- Hierarchical Clustering
- DBScan

### Neural Network types

- Reseau de neurones feed forward (FNN) *perceptron, convolutif*
- Réseau recurrent (RNN) *information bidirectionnelle*
- Réseau à résonance *très complexe, aquisition d'information sans changer les connaissances du réseau a un instant T*
- Réseau auto organisé *gestion des dimensions, carte auto-organisatrice de Kohonen*

### TP

a) Un perceptron multicouche (MLP, Multilayer Perceptron) : réseau de neurones artificiels organisé en plusieurs couches. Au moins trois couches de nœuds (entrée, une ou plusieurs couches cachées et sortie). Chaque nœud dans une couche présente une connection pondérée à tous les nœuds de la couche suivante. *Usage : apprentissage supervisé, classification & régression.*

b) Les fonctions d'activation :
    
- Fonction sigmoïde (sigmoid) : Transforme les valeurs en un intervalle entre 0 et 1 (classification binaire).
- Fonction tangente hyperbolique (tanh) : Transforme les valeurs en un intervalle entre -1 et 1. (couches cachées des réseaux de neurones).
- Rectified Linear Unit (ReLU) : Transforme les valeurs négatives en zéro et laisse les valeurs positives inchangées. (couches cachées, introduction de la non-linéarité).
- Fonction d'activation linéaire (Linear) : Fonction identité (couche de sortie pour les problèmes de régression).

c) Pour manipuler la taille des couches dans la fonction MLPClassifier de scikit-learn, le paramètre pertinent est hidden_layer_sizes. Il s'agit d'un tuple spécifiant le nombre de neurones dans chaque couche cachée. Par exemple, hidden_layer_sizes=(50, 25) indique deux couches cachées, la première avec 50 neurones et la deuxième avec 25 neurones.

d) Le "scaling" dans un réseau de neurones fait référence à la normalisation des valeurs d'entrée pour qu'elles soient sur une échelle comparable. Cela peut améliorer la convergence et la performance du modèle. On peut utiliser la fonction de normalisation z-score (standardisation), qui soustrait la moyenne et divise par l'écart-type des données. Dans scikit-learn, la classe StandardScaler peut être utilisée pour cela. Elle peut être appliquée aux données d'entrée avant d'alimenter le modèle de perceptron multicouche.

### Q learning

- Definir fonction action valeur Q
- Mettre a jour Q

### Livrables