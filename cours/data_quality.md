# Qualité des données


### Quoi
- ERP (data entreprise)
- Differents type (achat, production, logistique, finance, commandes, SI, gestion rh)

### Pour qui
- Exploitation economique de la donnée : energie, transport, batiment, santé
- Analyse comportementale client

### Metadata
- exiftool

### Piliers de la données
- Collecte
- Nettoyage
- Stockage
- Analyse

## Projet Radar

https://gitlab.com/Bob-117/data-quality/#radar

## Replication de base de données

https://cloudinfrastructureservices.co.uk/setup-mariadb-replication/ 

https://gitlab.com/Bob-117/data-quality#replication


### Master

#### Accès a la machine & la base base source

```sh
# Une serveur de prod distant
ssh user@host 
docker ps
xxxxxxxxxxxx   mariadb   "docker-entrypoint.s…"   5 hours ago   Up 5 hours   0.0.0.0:3307->3306/tcp, :::3307->3306/tcp   mymariadb
systemctl status maria
sudo mysql -h 0.0.0.0 -P 3307 -p
```

```sql
# sudo mysql -h 0.0.0.0 -P 3307 -p
SHOW DATABASES;
use radars;
SHOW TABLES;
SELECT * FROM radars LIMIT 10;
```

<details>
<summary>Manipulation</summary>

#### Configuration Master

```sh
nano /etc/mysql/mariadb.conf.d/50-server.cnf
```

```txt
bind-address           = 0.0.0.0
server-id              = 1
log_bin                = /var/log/mysql/mysql-bin.log
max_binlog_size        = 100M
relay_log = /var/log/mysql/mysql-relay-bin
relay_log_index = /var/log/mysql/mysql-relay-bin.index
```

```sh
systemctl restart mariadb
```

#### Verification

```sh
ss -antpl | grep mysql
```

```txt
LISTEN    0         80                 0.0.0.0:3306             0.0.0.0:*        users:(("mysqld",pid=3425,fd=22))
```

#### Creation du Replication User

```sql
CREATE USER 'bob_replication'@'%' identified by 'securepassword';
GRANT REPLICATION SLAVE ON *.* TO 'bob_replication'@'%';
FLUSH PRIVILEGES;
SHOW MASTER STATUS;
```

```txt
# keep this information for slave configuration
+------------------+----------+--------------+------------------+
| File             | Position | Binlog_Do_DB | Binlog_Ignore_DB |
+------------------+----------+--------------+------------------+
| mysql-bin.000001 |      786 |              |                  |
+------------------+----------+--------------+------------------+
```

```sql
EXIT;
```

#### Slave

```
nano /etc/mysql/mariadb.conf.d/50-server.cnf

bind-address            = 0.0.0.0
server-id              = 2
log_bin                = /var/log/mysql/mysql-bin.log
max_binlog_size        = 100M
relay_log = /var/log/mysql/mysql-relay-bin
relay_log_index = /var/log/mysql/mysql-relay-bin.index

systemctl restart mariadb

mysql -u root -p

STOP SLAVE;

CHANGE MASTER TO MASTER_HOST = '69.87.220.86', MASTER_USER = 'bob_replication', MASTER_PASSWORD = 'securepassword', MASTER_LOG_FILE = 'mysql-bin.000001', MASTER_LOG_POS = 786;

START SLAVE;
```

#### Verification


Sur master :

```sql
INSERT INTO radars.radars VALUES (1,"foo","bar");
```

Sur Slave : 

```sql
SELECT * FROM radars;
SHOW SLAVE STATUS \G
```


```txt
*************************** 1. row ***************************
                Slave_IO_State: Waiting for master to send event
                   Master_Host: 69.87.220.86
                   Master_User: replication
                   Master_Port: 3306
                 Connect_Retry: 60
               Master_Log_File: mysql-bin.000001
           Read_Master_Log_Pos: 1491
                Relay_Log_File: mysql-relay-bin.000002
                 Relay_Log_Pos: 1260
         Relay_Master_Log_File: mysql-bin.000001
              Slave_IO_Running: Yes
             Slave_SQL_Running: Yes
               Replicate_Do_DB:
           Replicate_Ignore_DB:
            Replicate_Do_Table:
        Replicate_Ignore_Table:
       Replicate_Wild_Do_Table:
   Replicate_Wild_Ignore_Table:
                    Last_Errno: 0
                    Last_Error:
                  Skip_Counter: 0
           Exec_Master_Log_Pos: 1491
               Relay_Log_Space: 1569
               Until_Condition: None
                Until_Log_File:
                 Until_Log_Pos: 0
            Master_SSL_Allowed: No
            Master_SSL_CA_File:
            Master_SSL_CA_Path:
               Master_SSL_Cert:
             Master_SSL_Cipher:
                Master_SSL_Key:
         Seconds_Behind_Master: 0
 Master_SSL_Verify_Server_Cert: No
                 Last_IO_Errno: 0
                 Last_IO_Error:
                Last_SQL_Errno: 0
                Last_SQL_Error:
   Replicate_Ignore_Server_Ids:
              Master_Server_Id: 1
                Master_SSL_Crl:
            Master_SSL_Crlpath:
                    Using_Gtid: No
                   Gtid_IO_Pos:
       Replicate_Do_Domain_Ids:
   Replicate_Ignore_Domain_Ids:
                 Parallel_Mode: conservative
                     SQL_Delay: 0
           SQL_Remaining_Delay: NULL
       Slave_SQL_Running_State: Slave has read all relay log; waiting for the slave I/O thread to update it
              Slave_DDL_Groups: 2
Slave_Non_Transactional_Groups: 0
    Slave_Transactional_Groups: 2
```
</details>


### RPGD

- Anonymisation
- PIA (Privacy Impact Assessment)


### Dropbox

innovation de rupture (AI) vs incremental