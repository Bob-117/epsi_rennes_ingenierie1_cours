# Intégration des données


## Intro

BI : Business Intelligence

Valorisation de la donnée
PowerBI

## Le système d'Information Décisionnel (SID)

- SI Gestion
- SI Industriel
- SI RH
- SI Décisionnel

### Prendre une décision

Acte Volontaire de faire ou ne pas faire

Choix, délibération, résolution

Prendr une décision en entreprise :

- Investissement (allouer des ressources a un projet)
- Gestion opérationnelle (Changement de SI)
-  Expansion (nouer des partenariats, explorer un nouveau marché)

Analyse statégique : Quoi, Qui, COmment, Quand, Ou, Combien => Prise de décision

Processus decisionnel :
- Reconnaitre la necessité d'une décision
- Indentifier alternatives
- Evaluer
- Choisir 
- Mise en oeuvre
- Evaluer les resultats


ROle du SID : Stocker, Traiter, Rendre accessible, Restituer sous forme de connaissance, Prise de Décision

Choisir la bonne donnée, Pondérer la donnée

DIK : Data => Information => Knowledge (donnée brute to connaissance établie)

Archi SID:

Data métier (ERP, CRM, RH, Finance, Spec Metier) => ETL (collecte, nettoyage, traitement) => Entrepot de données (grouper les données) 

![](../asset/sid-arch.png)


Entrepot de données, Datamart, ETL, metadata, OLAP, DashBoard, Sécurité

Intégration, Diffusion, Restitution

## Construire un SID

- Qui, quelles sont les données dont j'ai besoin
- Quoi, quel outils
- Ou, ou sont les données, d'ou ca vient
- Quand, a quelle fréquence, temps réel
- COmment, par quel intermédiare (le reseau, infra)
- Pourquoi, pour quels besoins
- Pour qui, quel public en a besoin

Source de données (fichers, app, api)

Sources : inaccessibles, incohérentes, obsolètes, non fiables, accessibles sans chiffrement

OUtils :

- ETL (Extraction, Transformation, Loading) => extait, tri, integre
- ELT (Extraction, Loading, Transformation)
- EAI (Enterprise, Application, Integration) => lien entre les appications en temps réel


SLA (quatre neufs 99.99%): garantit lv dispo d'un service

## Indicateurs cyber

Besoin d'un outil simple, accessible, temps réel

Identification (visuellement) les pts d'amélioration

Dashboard Pertinents centralisé, temps réel

Standardisation

### Notes

AS400, score esg, nvme, qos


## OLAP

Cube : stocker, collecter, calculer

Structure multidimensionnelle

ERP (SAP) => données collaborateurs (nom, prenom) => table en 2D => cube 3D


## Datacenter

Tier : niveau de service attendu

