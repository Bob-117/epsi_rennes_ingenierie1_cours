# Sécu info & RPGD *(Nicolas GENET)*

## RGPD 1
Loi : national

UE += directive et reglement

Directive : objectifs, voila ce qu'il faut faire, adaptable aux lois nationales

Reglement : quelques soient tes lois, tu fais ce que je dis *(eg: NIS2, DORA)*

RGPD : 
- Dispositions juridiques, organisationnelles et techniques
- Portée mondiale (traitement DCP ressortissant UE)

99 articles, Applicabilité 25 mai 2018

### Cheat Sheet (8 principes):
- Principe de licéité *(consentement, sauf sécurité nationale/vitale)*
- Principe de loyauté *(pas de traitement de mes données a mon insu)*
- Principe de limitation des finalités
- Principe de minimisation des données
- Principe d'exactitude *(droit de revision, maintien de données exactes et a jour)*
- Principe de transparence *(politique de confidentialité claire)*
- Principe de limitation de conservation *(pour des raisons reglementaires)*
- Principe de sécurité *(confidentialité, intégrité)*

### Responsable de Traitement
- Registre des traitements *(eg traitement X finalité A données abc)*
- Data Protection Officer *(personne physique)*
- **72h** déclaration violation de données *(confidentialité, intégrité, disponibilité)* + registre des violations

### Sous Traitant
- Registre
- Agit sur instruction documentée du RT
- Demontrer a tout moment la conformité RGPD
- **24h** *(dans la pratique, 72h tout compris)*

### Sanctions
- Financier : ca coule une petite boite, les Oracle, Microsoft, GAFAM s'en branlent *(+ils ont débauché les sachants de la CNIL avant le RGPD)*
- Avertissement (public ou non), Mise en demeure (publique)

### Interets légitimes
- Je prends toutes les données pour je sais pas quoi mais vu que je le dis ben je suis couvert

### Glossaire
- NIR : Numéro Sécu
- CNI : Carte Nationale d'Identité
- PAN : Primary account number
- CDO : Chief Data Officer
- STAD : Systeme de Traitement Automatisé de Données
- PVID : Prestataire de Vérification d'Identité à Distance
- PSSI : Politique de Sécurité du ou des Systèmes d'Information

### Dropbox
- https://www.data.gouv.fr/fr/datasets/?q=violation%20de%20donn%C3%A9es
- https://www.ssi.gouv.fr/entreprise/reglementation/confiance-numerique/le-reglement-eidas/ 

Le RGPD force a adapter la loi informatique et libertés (ne remplace pas)

Matomo pour l'utilisation des cookies

## RPGD 2

Intégrité, Confidentialité, Disponibilité, Tracabilité

### Outils a notre disposition

Anonymisation (!reversible) vs pseudonymisation (reversible)

- Algo : Sha-256, *(Bcrypt, Sha-1, Md5)*
- Chiffrement : AES-256 (sym), RSA (asym)
- HTTPS(SL) => TLS *Transport Layer Security*

#### Anonymisation : 

- Suppresion d'une colonne, irreversible, pas adapté aux données utiles
- Mise a blanc d'une colonne, irreversible, perte de cohérence
- Hashage / Chiffrement, irreversible, retracage impossible, clé a protéger
- Masquage / Troncature, reversible, recoupement possible
- Substitution, reversible, perte cohérence homogeneite
- Renumerotation, reversible, degradation qualité de la donnée
- Permutation, reversible, retracage possible


### Crypto
- Cryptologie : Etude scientifique d ela cryptographie et de la cryptanalyse
- Cryptographie : Ensemble des méthodes pour protéger des messages
- Chiffrement : Methodes qui permet de proteger des données incompréhensible sans la clé de (dé)chiffrement
- Cryptage : Terme incorrect souvent confondu avec chiffrement
- Key Lifetime / Crypto Periode : Durée de vie maximum d'une clé de chiffrement
- Cryptanalyse : Deduction d'un message ne clair a partir d'une message chiffré sans posseder la clé

Algo de chiffrement vs fonction de hashage = fonctions cryptographiques

### Authentification
- Connais
- Possede
- Suis
- Sais faire

PKI, IGC, Signature electronique, Condensat (empreinte), Certificat, Pseudo Random Number Generator

#### Sensibilisation et communication
- 30min - 1h présentiel *(/2 elearning)*

#### Implications opérationnelles du RGPD
- Identifier les tiers
- Contractualiser les exigences applicables
- Controler la conformité

#### Audit

Organisationnel, Archi, Code Source, Intruion, Configuration

- SonarQube : activer les plugin des sécurité

#### Sécu API
- Throttling (nb requete coté code != infra)
- Chiffrement du canal de communication
- Authentifier l'appelant
- Tracabilité des requetes
- Api Management (kong, wso2, happyG)
- SIEM IDS ?

#### Gerer les violations de données et réclamations
- 72h, sans délai pour les sous traitant (CNIL)
- Communication au cas par cas (sauf risque élevé)

#### Mise en situation : Audit

**App mobile** d'une enseigne de pret a porter, nombreuse **plaintes** un **SDK/pub embarqué** qui **géoloc** + **add MAC** en permanence, **ciblage pub** mais pas de violations. Nous sommes l'équipe en charge des **controles** de la CNIL.

- Installation/Utilisation, fonctionnement de l'application
- Lire la doc
- Visite
- Visio, Mail
- Internet, Archives, Dorks
- Les 2 DPO
- Est ce que la geoloc s'arrete qd je sors du magasin ?

|RT|ST|
|---|----|
|DPO|DPO|
|RSSI|Dev|
|Juriste (signataire contrat)|Architecte|
|_|_|
|Contrat|_|
|Registre|_|
|Doc|_|

- Etes vous au courant que vous avez une relation commerciale avec X et qu'elle constitue un traitement de données ?
- *oui*
- Montrer moi le registre de traitement
- *non*
- Est ce que vos utilisateurs sont au courant que vous collecter et traiter ? Case a cocher ? Tracabilité des consentements ?
- *oui ds les cgu*
- Non conformité transparence (& loyauté)
- Je comprends le ciblage publicitaire, ya a t il d'autre finalités ?
- Combien de temps vous garder les données ?
- *oui a des fins statistiques*
- Avez vous besoin de tout ca ?
- D'autres tiers ? OpenAI ? Transfert de données ?
- */!\ rester ds le périmètre*


#### Pression en tant qu'audité :
- Comportement auditeur
- Questions hors scope
- Absence de l'auditeur
- (Facturation / Frais suppl.)

*dropbox PMO, Directeur de projet / chef de projet*

#### Pression en tant qu'auditeur :
- Absence de l'audité
- Refus de réponse
- Pression de l'audité
- Corruption/chantage de l'audité
- Temporisation excessive de l'audité

si manque une réponse, a la restitution : ah ben on peut pas clore, on se revoit dans 1 mois.


#### Je vais auditer un sous traitant
- je peux auditer ? (clause auditabilité => delai de prevenance 10jrs ouvrés, scope=service rendu, frais, qui audit *neutre, conflit d'interet*, modalités de correction <-> contrat)
- Je prends contact avec la société et explique ma démarche
- Plan audti / ordre de mission
- Input infos/doc 
- Interview des acteurs identifiés
- Evaluation de la conformité a la lumiere des preuves communiquées
- Communiquer les premiers écarts (conforme, non conforme, *avec reserves*, majeur, mineur,info, remarques)
- Réunion cloture de l'audit
- Envoi du rapport d'audit + preuves complémentaires
- Décision finale

*dropbox CVSS*

En entretien d'audit : dis moi ce que tu fais, montre moi comment tu fais, montre moi que tu l'as deja fait


RGPD Application : 25 mai 2018

- Licéité (consentement)
- Loyauté (informer)
- Transparence (simple et claire)
- Minimisation
- Exactitude (intégrité, maj)
- Limitations finalités
- Sécurité

- Droit d'acces (a mes données)
- Droit de rectification (modifier)
- Droit à l'oubli (supprimer)
- Droit à la limitation (ne plus utilisaer mais conserver)
- Droit à la portabilité (récupération dans un format lsible)
- Droit d'opposition (à un objectif précis)
- Droit de refus d'une prise de décision automatisée (refuser de faire l'object d'une décision exclusivement produite par la machine)

### Privacy by design

Les données recueillies dans le cadre de ce parcours nous permettent de verifier votre idéntité. Le traitement de ces données est réalisé conformement au RGPD (lien) et loi informatique et libertés (lien).

En savoir plus sur la gestion de vos données et vos droits(lien).

Politique de confidentialité :

Dropdown :
- Objet du traitement des données
- Données traitées
- Personnes concernées
- Destinataires des données
- Durée de conservationdes données
- Sécurité (chiffrement, authentification forte (OTP one time password))
- Gestion des cookies
- Vos droits sur les données vous concernant

#### Cookies

### Chiffrement


AES (algo), 256 (taille de clé) (cf ecb, gcm), symétrique
RSA 4096

Hashage : MD5 (nope depuis 15 ans), SHA1 (nope depuis 10ans), SHA 256+, Bcrypt

=> Annexe B1 du RGS

Signature electronique
Condensat
Certificat
PRNG

PKI (Public Key Infrastructure)

Confidentialité, Intégrité, Disponibilité, Tracabilité



## Audit

Fin en 001 : Exigence

ISO 9001 (qualité)

ISO 14001 (environnement)

ISO 27001 (27 = sécurité, 27001:2013 ou :2022)

(ISO 25010 qualité logicielle)

Audit : indépendant, methodique, documenté (interne, externe privé, externe public ie CNIL)

Relever des Preuves d'audit : Documents, "Montrez moi", Entretien, Screen

Audit dans un env iso prod

Auditeur + [ Commanditaire + Audité ]

KPMG (Big4)

Audit, pentest : trouver des vulnérabilités et des écarts (assurer sécurité et conformité)

Bloquant Critique Majeur Mineur Info

5 types audit sécu : 
- [x] Intrusion (pentest)
- [x] Sécurité de code source
- [x] Configuration
- [ ] Architecture
- [ ] Organisationnel

+ performances (stress test => disponibilité, test pas audit)

Score CVSS (common vuln scoring syst)

Top 10 OWASP

OWASP DEPENDENCY CHECK

OWASP ZAP

SNYK.io

BURP SUITE

ISO 19011:2018 : Méthode normée d'audit

(HS : CICD ~ Plateforme d'intégration continue, DevOps, DevSecOps, NoCode)

#### Audit

- [x] Declenchement (faisabilité? prise de contact ?)

- [x] Préparations des activites (revue documentaire)

Exemple : lire le DAS (explication du systeme, conception du systeme)

archi (conceptuel, logique, phyisique = pourquoi comment avec quoi, voir archi capge http://architectureportal.org/capgemini-integrated-architecture-framework-iaf)

Matrice de flux

SFG SFD

- [x] Réalisation des activités d'audit

ISO:00000

|jour |h    | quoi            | avec qui                       |
|-----|-----|-----------------|--------------------------------|
|Lun  | 9h00| café            | tout le monde                  |
|Lun  | 9h20| x               | abcd                           |
|Lun  |18h00| Restitution     | tout le monde, surtout le chef |

Plan, guide, on sait ce qu'on veut auditer

1. Dis moi comment tu fais
2. Montre moi comment tu fais
3. Montre moi que tu l'as fait

=> Respecter son scope, son périmètre

- [x] Préparation et diffusion du rapport d'audit


- [x] Cloture de l'audit

Profil : Technique, mediateur, impartial, courage

Les conclusions d'audit reposent sur les constatations

3 roles pendant l'audit :

Responsable d'equipe audit + auditeurs

- Correspondant d'audit = interlocuteur principal, guide
- Observateur (Client/Commanditaire/Autorités )= s'assure que tout se passe bien (l'auditeur arrive dans l'entrepot, s'assure qu'il met ses chaussures de sécu)
- Expert (technique)

## Evaluation

1. Suis dans mon droit d'auditer cette société
2. Je prends contact avec la société et explique ma démarche
3. J'envoie un ordre de mission et/ou plan d'audit
4. je recueille en amont de la visite un maximud d'infos/doc
5. J'interview chacun des acteurs identifiés
6. J'evalue la conformité à la lumiere des preuves communiquées
7. Je communique les premiers écarts constatés
8. Réunion de cloture de l'audit
9. J'envoie le rapport d'audit une fois terminé et recueille éventuellement quelquels preuves complémentaires
10. Décision finale (tendance annoncée lors de la réunion de cloture)

(Annonce 10 jours ouvrés, contre audit (+ court) si mise en demeure)

Vous etes repsonsable d'une équipe d'audit en charge de controler la conformité RGPD d'un sous traitant de votre client.

Le sous traitant audité est un éditeur de logiciel SIRH (congés, paie, recrutement => données peronnelles).

Suite a un incident de sécurité chez l'éditeur dans un autre périmetre, votre client à de fortes inquiétudes vis a vis de la sécurité des DCP confiées à ce sous traitant dans le cadre de la souscription à la solution

4 jours homme ont donc été dégagés par le client pour cet audit qui se fera intégralement sur site.

### Correction :

- Mail de contact : 

Objet, on fait pas la morale, pas de patos (on est pas la pour rassurer ni pour affoler), mentionner la clause d'auditabilité, appel téléphonique ++, mentionner plus de détails sur les finalités de l'audit (sur quoi), proposer une période (pas imposer une date)

- 10 Questions :

Questions ciblées, pas trop vastes, quels types de dcp ? cdp sensibles soumises RGPD ? Avez vous nommé un DPO ou une personne responsable de la conformité RGPD au sein de l'entreprise ? Quels moyens alloués ? Avez vous realisé une analyse d'impact ? Parlez des sous traitant de seconde ordre/sous traitant ultérieur. (DPO, les data qui sont traitées, ou sont les data, secu stockage, jeux de test). AVez vous deja été audité ? recemment ?

Plan d'audit : acteurs : DPO, rssi, dev, admin, archi, utilisateurs de l'outil


## Security by Design

Penser un système en intégrant la sécurité dès la conception

- User input

Modèle PDCA (ou route de Deming) :

- Plan (définir les exigences de sécurité)
- Do (mettre en oeuvre les exigences de sécurité)
- Check (Vérifier la bonne mise en application automatisé ou manuel : *pentest, code review, audit de code source, SonarQube*)
- Act/Ajust (améliorer si necessaire)

### Sécurité applicative

Modèle en couche : 
- Couche politique et procédure (pssi, sensibilitation)
- Couche physique (badge exctincteur caméra)
- Couche périmetrique (parefeu dmz)
- Couche réseau (lan vlan vpn)
- Couche system (antivirus chiffrement disk)
- Couche application (secure coding, configuration)
- Couche données (chiffrement en base, anonymisation)

Ecosysteme
- Roles : RSSI DPO
- Autorité : ANSSI, CNIL, ARJEL (autorité jeux en ligne) => Aléatoire => PRNG (pseudo random number generation)
- OWASP, CVE, StackExchange (https://security.stackexchange.com/)


Secure Software Development Lifecycle (S-SDLC)
- Normalisation (formalisation des règles de dev, guides, exemples : OWASP top10, SANS top25 most dangerous software errors)
- Intégration (dans le code, au moyen d'outils dédiés, framework, api, librairies)
- Vérification (de l'implémentation effective de ces règles, auto ou manuel) => OWASP Dependency check
- Maintien (en condition de sécurité pour s'assurer de l'efficacité dans le temps)


### Le con coin

Features:
- Chercher article
- MEttre en vente
- Contacter
- Acheter
- Avis
- Poster annonce
- CRUD account
- Choix point de livraison

Attaques:
- Faux compte
- Anonyme
- Brute force
- Vendre produt défaillant
- Modifier état d'un article pour le vendre plus cher

Solution:
- KYC (know your customer) => vérification mail telephone cni
- Prise de vue en temps réel, video
- Figer annonce après achat

Controles:
- Tests U, fonctionnels, intégration
- Tests de bout en bout
- Recette usine & client
- Controles de sécurité (analyse statique, dynamique, vérification des dépendances *OWASP Dependency check, Snyk.io*)


Référentiel:
- Mitre, CVE : indentifier
- CVSS : scoring
- OWASP : detecter

Controle d'accès = authentification + habilitation

- https://en.wikipedia.org/wiki/VeraCrypt
- https://en.wikipedia.org/wiki/TrueCrypt

### 8 fevrier 2024

Actu : Leak 33 millions numéro sécu

Données personnelles vs données sensibles (l’origine raciale ou ethnique, les opinions politiques, les convictions religieuses ou philosophiques; l’appartenance syndicale; les données génétiques, les données biométriques traitées uniquement pour identifier un être humain les données concernant la santé; les données concernant la vie sexuelle ou l’orientation sexuelle d’une personne; instruction judiciaire / juridique.)

5 cas ou je m'affranchis du consentement : interet vital, interet de la nation, etablissement d'un contrat (on a besoin de savoir qui est en face)

Limitation de la conservation : durée fixée selon reglementation et contexte

Test d'intrusion (pentest) : black, grey, white box (white box j'ai le code source, cred admin etc)

Pour mercredi 14 fevrier : audit CNIL

![eval](../evaluation/rgpd_eval.png)
