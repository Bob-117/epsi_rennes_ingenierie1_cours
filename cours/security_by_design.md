# Security By Design

## Analyse de risques

- Métriques traditionnelles

    - Besoins en confidentialité (Public Interne Restreint Confidentiel)

    - Besoins en intégrité (Signalement, Signalement & Correction, Intégrité à l'Utilisation, Intégrité Garantie)

    - Besoins en disponibilité (+72h, 24/72h, 8/24h, -24h)

    - Echelle de gravité (Négligeable, Limitée, Importante, Critique)

    - Echelle de Vraisemblance (Très peu, peu, vraisemblable, très vraisemblable)


*Passbolt (gestionnaire mot de passe pour équipe)*

### Source de risque

|         | Source               | Type         | Objectif visé                                             | Motivation | Capacité | Pertinence | Commentaires       |
|---------|----------------------|--------------|-----------------------------------------------------------|------------|----------|------------|--------------------|
| Externe |                      |              |                                                           |            |          |            |                    |
|         | Script Kiddies       | Intentionnel | Montrer ses "exploits"                                    | ++         | +        | Moyenne    |                    |
|         | Hacker               |              | Défi, financières                                         |            |          |            |                    |
|         | Cybercriminels       |              | Vols infos en vue d'actes malveillants                    |            |          |            |                    |
|         | Concurrent           |              | Vols infos pour avantage concurrentiel                    |            |          |            |                    |
|         | Hacktivist           |              | Divulgation infos compromettantes, motivation idéologique |            |          |            |                    |
|         | Media                |              | Divulgation infos sensibles / compromettantes             |            |          |            |                    |
|         | Gouvernement         |              | Espionnage des activités stratégiques                     |            |          |            |                    |
|         | Malware              |              |                                                           |            |          |            |                    |
|         | Sinistre             | Naturel      |                                                           |            |          |            |                    |
| Interne |                      |              |                                                           |            |          |            |                    |
|         | Opérateur            | Intentionnel | Motivation pécuniaire, Vengeance                          |            |          |            |                    |
|         | Opérateur            | Accidentel   |                                                           |            |          |            |                    |
|         | Développeur          |              |                                                           |            |          |            |                    |
|         | Hebergeur            |              |                                                           |            |          |            |                    |
|         | Personnel temporaire |              |                                                           |            |          |            | Ménage, Stagiaires |

### Analyse de risque

- Périmètre
- Métriques
- Lister des biens sensibles (base client) et biens supports associés (serveurs, humains)
- Exprimer ses craintes (scénarios de menace, liste event redoutés, EBIOS RM)
- Evaluer les risques (et choix d'action)
- Présenter les résultats de l'analyse è un décideur

### Schéma Architecture

- Physique
- Reseau
- Logicielle
- Données

Les 3 types / vues d'archi (CapGemini) : physique (avec quoi), logique (comment), conceptuelle (quoi)

### Mise en application


#### Cas 1

Developper et integrer un SDK a une application mobile native de home banking existante. Le SDK permet d'identifier a distance avec une carte d'identité un futur client au moment de la création de son compte pour eviter un déplacement physique. (lutte contre la fraude, sécurité, RGPD).

EN TANT QUE
JE SOUHAITE
POUR

User Input = danger *particulièrement Upload*

Usurpation d'identité
Injection de code dans la bande mrz

#### Cas 2

Evil Stories application le bon endroit (le con boin like)









