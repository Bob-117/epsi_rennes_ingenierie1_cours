# Big Data


## Intro

Debut 2000, les GAFAM, Réseaux sociaux

Data : scale, diversity, complexity

New architecures, techniques, algo & analytics (manage, extract, exploit)

Initialement : Volume, Variety, Velocity

Aujd : Veracity, Value (suite à problemes de gouvernance de la donnée, datalake, stocker et retrouver)

Historiquement : Hadoop (tech open source)

Enjeux : Stocker, Paralelliser, cluster, traitement (fondamental : MAP REDUCE)

- 1996 : Yahoo
- 2003 : Itunes, Android, Steam, Skype, Tesla
- 2004 : Thefacebook, Gmail
- 2006 : Youtube (recommandations)

Publications : 

- https://en.wikipedia.org/wiki/Google_File_System
- https://static.googleusercontent.com/media/research.google.com/fr//archive/mapreduce-osdi04.pdf
- https://static.googleusercontent.com/media/research.google.com/fr//archive/bigtable-osdi06.pdf

Doug Cutting : Hadoop (HDFS Hadoop Distributed File System) + MapReduce

APache Kafka par linkedin, gestion evenements.

AUjd : Apache Spark

https://netflixtechblog.com/

https://www.getdbt.com/


Historique : 3V + 2V => Hadoop => HDFS & MapReduce => Adoption (open source) => SQL & Spark => Cloud 

## Big Data Problem

- CPU (Moore Law ~2005)
- Parallelism 
- volume disque ++ => failure ++
- Data locality (network, colocality, proximité entre la data et le cpu qui la traite)
- Limit HDFS replication (blast)

## Map Reduce

![map_reduce](../asset/big_data_map_reduce.png)

Process & ecriture lecture recursif 

## Spark

RApidité, pas que traité des mot mais data structurée (img ?) + passage biga data (cluster)

sparkebyexample

Split c'est bien, attention au Skewing (déséquilibre entre les charges des différents workers)

## Roles

- Data Architect (choisit spark)
- Data Engineer (pipeline ETL)
- Data Scientist
- Data Analyst

Data > Information > Knowledge > Insight > Wisdom

Processus ETL ou ELT

## Data Lake

Raw Data > Trusted Data > Service Data


https://static.googleusercontent.com/media/research.google.com/fr//pubs/archive/36632.pdf

## Streaming

Différent des (data = gros fichiers stockés et traités)

Pub/Sub (Kafka)
