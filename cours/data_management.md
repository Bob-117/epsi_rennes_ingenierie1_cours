# Data Management

- https://www.youtube.com/watch?v=XwF_b_mKn7k
- https://www.youtube.com/watch?v=OjycIbXZGuI 

Stockage de moins en moins cher => de plus en plus de données, moins de limites (réplication), facilité d'accès

Le Data Managment :
- Connaissances des données de l'entreprises *(type, contenu, domaine)*
- Connaissances des technos pour gerer ces données *(stockage, accès, protection)*
- Connaissances des différents acteurs ayant des activités en lien avec une donnée et mise à disposition le cas échéant *(CRUD)*

Collecte, Stockage, Transformation, Tri, Analyse, Distribution, Présentation, Exploitation

Performance, Domaine, Discipline


## Nasa Data Management

https://gitlab.com/Bob-117/nasa-data-management

![](../asset/data_management_process.png)

## Home assistant

### Collecte des données

Installation de la VM (1 par groupe accessible par les membres du groupe)

https://www.home-assistant.io/installation/linux (Mon VirtualBox pue, on prend le VMWare d'Orel)

Dans cette VM, il y a un gestionnaire docker, on peut deployer des Addon (ie des containers docker dans la VM).

On ajoute les Addon SQLLiteWeb et FileEditor.

Avec l'Intégration (!= addon) SensorCommunity, on va chercher un capteur sur https://maps.sensor.community/# et dans l'idée, quand la pollution est elevée HomeAssistant ferme les fenetres.

Avec FileEditor > Browse > `/config/configuration.yaml` on ajoute d'autres intégrations a la main.


<details>

<summary>Restful Integration :</summary>

```py
import os
import requests

api_key = os.getenv("NASA_API_KEY")
url = "https://api.nasa.gov/planetary/apod"

response = requests.get(url, params={"api_key": api_key})

if response.status_code == 200:
    data = response.json()
    print(data)
else:
    print("Error:", response.status_code)
```

```python
# Output
{'date': '2023-10-17',
 'explanation': "It's not the big ring that's attracting the most attention. "
                'Although the big planet-forming ring around the star PDS 70 '
                "is clearly imaged and itself quite interesting. It's also not "
                'the planet on the right, just inside the big disk, that’s '
                'being talked about the most.  Although the planet PDS 70c is '
                'a newly formed and, interestingly, similar in size and mass '
                "to Jupiter. It's the fuzzy patch around the planet PDS 70c "
                "that's causing the commotion. That fuzzy patch is thought to "
                'be a dusty disk that is now forming into moons -- and that '
                'had never been seen before. The featured image was taken in '
                '2021 by the Atacama Large Millimeter Array (ALMA) of 66 radio '
                'telescopes in the high Atacama Desert of northern Chile.  '
                'Based on ALMA data, astronomers infer that the moon-forming '
                "exoplanetary disk has a radius similar to our Earth's orbit, "
                'and may one day form three or so Luna-sized moons -- not very '
                "different from our Jupiter's four.",
 'hdurl': 'https://apod.nasa.gov/apod/image/2310/PDS70_ALMA_1237.jpg',
 'media_type': 'image',
 'service_version': 'v1',
 'title': 'PDS 70: Disk, Planets, and Moons',
 'url': 'https://apod.nasa.gov/apod/image/2310/PDS70_ALMA_960.jpg'}
```

Dans `configuration.yaml` : 

```yaml
sensor:
  - platform: rest
    name: "NASA APOD"
    resource: https://api.nasa.gov/planetary/apod
    method: GET
    scan_interval: 86400
    headers:
      Content-Type: application/json
    params:
      api_key: !secret nasa_api_key
    value_template: "{{ value_json.date }}"
    json_attributes:
      - date  # On connait ces clés grace a la requete python précédente
      - explanation
      - Hdurl
```

</details>


### Stockage des données

Migration vers MariaDB : https://theprivatesmarthome.com/how-to/use-mariadb-instead-of-sqlite-db-in-home-assistant/

### Tri des données

Comprendre la donnée : https://www.tim-kleyersburg.de/articles/longtime-statistics-home-assistant/. On fait des requetes SQL et on parcourt les tables.

![](../asset/data_management_home_assistant_data_struct.png)

### Analyse

On installe l'Addon Grafana. Pour connecter la datasource, il faut renseigner le nom du container (docker, puisque maria est un Addon) `core-mariadb:3306` + creds.

![](../asset/grafana.png)


InfluxBD : https://www.youtube.com/watch?v=8t72hjsmn_g & https://www.youtube.com/watch?v=aXRxk9IEEQU

