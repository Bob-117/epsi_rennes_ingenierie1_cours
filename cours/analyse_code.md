# Analyse de code & Maintenabilité

## Complexite

Tout ce qui contribue a rendre difficilela compréhension et la modificztion d'un systeme

Simple : objectif, clair et cohérent

Facile : subjectif

Complexe != compliqué

Racine de ts ls pb : non confiormité, incapacité a modifier le systeme, couts, failles sécu, mauvaises perfs

- Compréhension (lecture)

A partir d'un petit nb des parties d'un systeme, isoler + raisonner =  analyser

## Comprendre

Prérequis, comment peut on comprendre un systeme ?

- Depuis l'exterieur avec des tests (boite noire, resultats d'observations sur un comportement observé dans des conditions spécifiques)
- Depuis l'interieur : raisonnement informel (RI), lire le code, commentaires, doc, on examine le systeme en regardant/inspectant ses parties

RI est plus important, les tests sont plus limités (detection d'erreur), le RI = "creer moins d'erreurs"

Caracteristiques d'un bug : il passe les tests

Pb des tests : nous informe sur un comportement pour un set d'input particulier, ne dit rien si on fournit un autre set d'input (on peut pas etre exhaustif), est ce qu'on fait les bons tests ?

Dijkstra : les tests sont utiles pour detecter "hopelessly inadequate" les erreurs, ca ne montre ou prouve pas qu'il n'y a pas d'erreur

/!\ Tests utiles, filet de sécurité mais pas suffisant

Tests + RI

On recher la Simplicité : difficile

## Causes (fondamentales)  de la complexité

- Les etats (mutables) : une donnée (valeur) stockée par un composant d'un programme _a un moment donné_. Cette donnée va determiner le comportement du composant. Concretement : contenu de la memoire (RAM, Disque, registre) a un instant donné au cours de l'execution du programme.
Référence a une valeur dont on va avoir besoin dans le futur.

Mutable : modification de la valeur

(Un systeme sans etat mutable existe, on y vient plus tard

- Flot de controle 

- Volume de code (effet d'échelle, + grand + compliqué)

Ces 3causes ont un impact sur les tests et le raisonnement (RI)

### Impact des etats mutables sur les tests

Redemarre, Reinstalle, Reinstalle l'OS, Essaie encore: le systeme retrouve un etat stable et ca fonctionne, retrouver UN bon etat. (Grand nb d'etat possible => raisonnement compliqué, tests dans un etat donné ne disent rien sur le comportement du systeme dans unautre etat).
Etat initial caché (initialisation) + les etats mutables se combinent aux effets des inputs.

### Impact des etats mutable sur le RI

Simulation mentale du comportement du module/bout de code pour comprendre le fonctionnement: plus il y a d'etat, + configuration, + chemin possible d'execution. Impossible de raisonner, nb scénario explose. (0,1 | 00,01,10,11 | 2^n)

### Flot, Flux de controle, d'execution

Flot : ordre ds lequel les instructions sont executées (séquences)
Tout le tp en train de gerer un ordre meme si pas d'importance. Langage imperatif => ordre d'execution (du code)

Le code SQL est juste déclaré, le moteur le traite (pas deflot de controle)

Surspécification du pb, imposer ordre/archi

Impact sur le RI : savoir si le flot encode un choix de design ou s'il est sans importance. Pb de décomposition temporelle.


### Volume de code

SUpprimer du code

### Autres

- code source: Duplication de code,code mort,mauvaise abstraction,mauvaise modularité
- env : mauvaise absence doc,mauvaise analyse pb
- La complexité engendre la complexeite (incremental, contamination)
- Simplicité est difficile
- Le pouvoir corrompt, langage permissife,langage puissant systme difficile a comprendre

## Etude de cas

- https://github.com/paul-schuhm/refactoring-starter

- Trop de fonctionnalités
- Nb magiques (constantesen dur)
- Switch, Contamination, manque abstraction
- Tableau PHP (inéhrent au langage)

Abstraction = masquer les details

### Gerer la complexite

- Approche classique : 
	- imperatif (procedural, POO) : accès aux attributs (mutables), identité (print(id(myobj)), tout object est uniquement identifiable (object identity, intentional identity = ref memoire)
	- programmation fonctionnelle : Clojure (Rich Hickey, Lisp), Scheme, Haskell
	- logique / declaratif (SQL)

Modularite

Ideal : Utiliser simple a tester (fonctionnel, logique) et interaction exterieur (procedural, gestion des etats mutables, flow de control, c'est le client)

Complexité accidentelle et essentielle 

Essentielle : irreductible,appartient au pb metire a resoudre. Vue par le user (spec) 

Accidentelle : tout complexite restante a laquelle le dev ne devrait pas etre confronté dans un monde idéal, complexité non necessaire mais presente (introduite a l'implementation)

### Resumé

- Maintenabilité et complexité (lire, comprendre, modifier)
- 3 causes fondamentales : etat mutables, flot de controle, volume de code
- La plupart de la complexité est accidentelle
- Chaque paradigme (fonctionnnel, poo...) est affecté par la complexité
- Separer etats mutables de la logique pure => pas facile de faire simple
- Choisir des langages plus faibles, adapté pour chaque module/partie
- Performances : + simple d'ameliorer les perf d'un systeme lent designé/concu pour etre simple (modularité) que de retirer de la complexité a un systeme designe pr etre rapide

## Refactoring

Changement apporté a la structure interne d'un systeme -> +simple a comprendre et modifier, SANS changer le comportement (externe) observé. Petits chgts, incrémental. Temps execution, cout, memoire, optimisation.

#### Pourquoi Restructurer, réarchitecturer, refactorer ? 

- Ameliorer structure, design, arch (le systeme en perd au cours du temps, "short dev", "features"
- Mauvais dev : + de temps pour dev, debug, + code + bug
- Programmer + vite
- Integrer le refacto dans le workflow de travail

#### Quand ?

- on doit apporter un changement, avant de changer => comprendre => compréhension "suffisante"
- un bout de code est vraiment mauvais (si on laisse vivre c'est critique)

#### Quand on s'arrete ?

- Jamais parfait (prends de l'energie, du temps)
- YAGN You aren't gonna need it
- Faire unc compromis
- Incrémental

Refacto != clean, nettoyer, il y a une intention derriere, il faut garder ca court (branche + test vert commit)

#### Quand ne pas refactorer

- Pas de modification
- Parfois c'est plus simple de tout réécrire

#### Notes

- TDD, commit, Kent Beck

