# Data Mining

## Intro

mmds.org

Extraire de la donnée : data => information => décision

Identification de pattern, corrélation variables composites

*(feature : Stockage analogique, magnétique)*

L'age d'or du big data : Co evolution Storage capacity, transmission capacity, processing capacity

*Notebook fr : Husprey*

Pattern et model : Valid, Useful, Unexpected or non obvious, Understandable, Complete

- **Description : Clustering**
vs
- **Predictif : Systeme de recommandation**

Descriptif : what happend (report, bi)
Diagnostic : Why did it happend (rca https://www.mindtools.com/ag6pkn9/root-cause-analysis, correlation, causation)
Predictive : What will happend (prediction)
Prescriptive : How to make this happend (recomandation)

Characterization vs Discrimination

Data mining goals : Produce a model, create a summary, extract proeminent features

Spurious pattern (Bonferroni principle)

**Transactionnel vs Analytique**

Transactionnel : database, ACID, commit, atomique, *Ligne orienté : INSERT INTO retrait 100; UPDATE acc WHERE id=117 (indexé)*

Analytique : DataWareHouse, DataMart, Chargement Bulk (en masse), Exploitation (SELECT max/min/avg GROUP BY customer_35) *TeraData (MPP), (H)exaData (hp), RedShift (Amazon), SnowFlake (Tourne sur les 3 clouds Amazon Azure Google), BigQuery (Google), Synapse (Azure)*

Data Cleaning, Integration, Selection, Transformation, Mining (Algo), Pattern Evaluation, Knowledge Presentation

## Types

### Data type

- Comment on traite (algo), encode, présente

- Dependant vs Indépendant (time series, graph)

- Structuré : table
- Semi Structuré : json
- Non Structuré : text img audio video

- One Hot Encoding, Embedding

- Contextual (time) vs Behavioral (value, sensor)

- Spatial Data (lat/lon) *Postgres GIS*

### Problem type

- Relationship 
    - Columns (associations, correlations, classification, prediction)
    - Rows (Cluster, outliers)

- Pattern *{A} => {B}, {Chips, Olives} => {Beer}*, Frequency

https://medium.com/@cxdai99/alpha-house-vs-house-of-card-how-data-analysis-helped-netflix-but-not-amazon-92c71eb55b3e


### Data exploration

Manipulation (list, array, col, row, numpy, pandas)

Extraction, casting, cleaning, integration, reduction, selection, transformation, conversion.

Données en silot (integration)

Sampling, cohérence

### Roles
- Data Engineer (pipeline)
- Data Scientist (mining, ML/AI)
- Data Analyst (report, sql)
- Data Stewart (gouvernance/catalogue, quality)
- Data Architect
- Chief Data Officer
- Data Protection Officer

